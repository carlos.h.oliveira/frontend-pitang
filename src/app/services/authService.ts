import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BASE_URL } from '../services/api';
import { LocalUser } from '../domains/localUser';
import { Credentials } from '../domains/credentials';
import { StorageService } from './storageService';
import * as jwt_decode from 'jwt-decode';
import { User } from "../domains/user";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private userIdentity: any | null = null;
    public resourceUrl = BASE_URL;

    constructor(
        public http: HttpClient,
        public storage: StorageService) {
    }

    httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

    authenticate(creds : Credentials) {
        return this.http.post(
            `${this.resourceUrl}/signin`,
            creds,
            {
                observe: 'response',
                responseType: 'text'
            });
    }

    successfulLogin(authorizationValue : string) {
        let tok = authorizationValue.substring(7);
        let user : LocalUser = {
            token: tok,
            login: jwt_decode(tok).sub
        };
        this.storage.setLocalUser(user);
        this.userIdentity = user;
    }

    logout() {
        this.storage.setLocalUser(null);
    }

    me() {
        return this.http.get(`${this.resourceUrl}/me`);
    }

    isAuthenticated(): boolean {
      return this.userIdentity !== null;
    }
}
