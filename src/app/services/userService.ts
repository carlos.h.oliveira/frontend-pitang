import { BASE_URL } from '../services/api';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../domains/user';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public resourceUrl = BASE_URL;

    constructor(private http: HttpClient) { }

    findAll(): Observable<User[]> {
        return this.http.get<User[]>(`${this.resourceUrl}/users`);
    }

    findById(id: number): Observable<User> {
        const url = `${this.resourceUrl}/users/${id}`;
        return this.http.get<User>(url).pipe(map((res: any) => this.convertDateFromServer(res)));
    }

    delete(id: number) {
        return this.http.delete<any>(`${this.resourceUrl}/users/${id}`,
            {
                observe: 'response'
            });
    }

    save(user: User): Observable<any> {
        return this.http.post<User>(
            `${this.resourceUrl}/users`,
            user
        );
    }

    update(user: User): Observable<any> {
        return this.http.put<User>(`${this.resourceUrl}/users/${user.id}`, user, {
            observe: 'response'
        });
    }

    protected convertDateFromServer(res: any): any {
        if (res.birthday) {
            res.birthday = res.birthday != null ?
                moment(res.birthday, 'YYYY-MM-DD').toDate() : null;
        }
        return res;
    }

}
