import { BASE_URL } from '../services/api';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Car } from '../domains/car';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CarService {
    public resourceUrl = BASE_URL;

    constructor(private http: HttpClient) { }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    findAll(): Observable<Car[]> {
        return this.http.get<Car[]>(`${this.resourceUrl}/cars`, {});
    }

    findById(id: number): Observable<Car> {
        const url = `${this.resourceUrl}/cars/${id}`;
        return this.http.get<Car>(url, {});
    }

    delete(id: number) {
        return this.http.delete<any>(`${this.resourceUrl}/cars/${id}`,
            {
                observe: 'response',
            });
    }

    save(car: Car): Observable<any> {
        return this.http.post<Car>(
            `${this.resourceUrl}/cars`,
            car
        );
    }

    update(car: Car): Observable<any> {
        return this.http.put<Car>(`${this.resourceUrl}/cars/${car.id}`, car, {
            observe: 'response'
        });
    }

}
