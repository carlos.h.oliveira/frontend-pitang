export class Car {
    id: number;
    year: number;
    licensePlate: string;
    model: string;
    color: string;
    user: User;
    img?: string;
    file?: string;
  }
  
  class User {
      id: number;
  }