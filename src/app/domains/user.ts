import { Moment } from 'moment';
import { Car } from './car';

export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    birthday: Moment;
    createdAt: Moment;
    lastLogin?: Moment;
    login: string;
    password: string;
    phone: string;
    cars: Car[];
    img?: string;
    file?: string;
  }