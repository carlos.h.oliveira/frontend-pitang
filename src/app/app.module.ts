import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { SigninComponent } from './components/signin/signin.component';
import { UsersComponent } from './components/users/users.component';
import { CarsComponent } from './components/cars/cars.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MeComponent } from './components/me/me.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogService } from './components/confirm-dialog/confirm-dialog.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RootComponent } from './components/root/root.component';
import { ToastrModule } from 'ngx-toastr';
import { ModalComponent } from './components/modal/modal.component';
import { UserRoutingModule } from './components/users/user.routing';
import { CarRoutingModule } from './components/cars/car.routing';
import { NewCarComponent } from './components/new-car/new-car.component';
import { AuthInterceptorProvider } from './interceptors/authInterceptor';
import { AuthExpiredInterceptorProvider } from './interceptors/auth-expired.interceptor.ts';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    SigninComponent,
    UsersComponent,
    CarsComponent,
    MeComponent,
    ConfirmDialogComponent,
    RootComponent,
    ModalComponent,
    NewCarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    NgbModule,
    AngularFontAwesomeModule,
    CommonModule,
    UserRoutingModule,
    CarRoutingModule,
    ToastrModule.forRoot()
  ],
  providers: [ConfirmDialogService, NgbActiveModal, AuthInterceptorProvider,
    AuthExpiredInterceptorProvider],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [ConfirmDialogComponent, ModalComponent],
})
export class AppModule { }
