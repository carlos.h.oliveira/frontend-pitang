import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/domains/user';
import { Car } from 'src/app/domains/car';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/userService';
import { Title } from '@angular/platform-browser';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { NotificationService } from 'src/app/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user = new User();
  car = new Car();
  cars = [];

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
    private confirmDialogService: ConfirmDialogService,
    private rootComponent: NotificationService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(({ user }) => {
      this.user = user;
      this.cars = user.cars;
    });
    this.title.setTitle(
      this.user.id
        ? `Updating user - ${this.user.firstName}`
        : 'New User'
    );
  }

  saveUser() {
    this.user.cars = this.cars;
    if (!this.user.id){
      this.userService.save(this.user).subscribe(
        () => {
          this.user = new User();
          this.cars = [];
          this.rootComponent.showSuccess("User added succesfuly")
        },
        error => {
          this.rootComponent.showError(error.error.message);
        }
      );
    } else {
      this.userService.update(this.user).subscribe(
        () => {
          this.rootComponent.showSuccess("User updated succesfuly")
        });
    }
  }

  addCar() {
    if (this.car.year && this.car.model && this.car.licensePlate && this.car.color) {
      if (this.cars === undefined)
        this.cars = [];

      this.cars.push(this.car);

      this.car = new Car();
      this.rootComponent.showSuccess('Car added');
    }
  }

  removeCar(car: Car){
    const index = this.cars.indexOf(car);
    this.cars.splice(index, 1);
  }

  editCar(car: Car){
   this.car = car;
  }

}
