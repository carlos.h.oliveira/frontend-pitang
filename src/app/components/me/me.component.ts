import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/authService';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.css']
})
export class MeComponent implements OnInit {
  user: any;
  retrievedImage: boolean = true;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.me().subscribe(
      data => {
        this.user = data;
      }
    );
  }

  getImageUrl(): string {
    // if (this.user && this.user.img) {
    //   const imageBlob = new Blob([this.user.img], { type: 'image/jpeg' });
    //   this.retrievedImage = true;
    //   return URL.createObjectURL(imageBlob);
    // }
    return 'assets/img/avatar.png'; // Retorne uma URL de imagem padrão ou vazia, se não houver imagem
  }

}
