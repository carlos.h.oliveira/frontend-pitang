import { Component, Input, OnInit } from '@angular/core';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Car } from 'src/app/domains/car';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() title;
  @Input() content;

  cars: Car[];

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.cars = this.content;
  }

}
