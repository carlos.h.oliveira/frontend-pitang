import { Component, OnInit } from '@angular/core';
import { Car } from 'src/app/domains/car';
import { CarService } from 'src/app/services/carService';
import { NotificationService } from 'src/app/notification.service';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars: Car[];

  constructor(
    private carService: CarService,
    private rootComponent: NotificationService,
    private confirmDialogService: ConfirmDialogService
  ) { 
    
  }

  ngOnInit() {
    this.carService.findAll().subscribe(
      data => {
        this.cars = data;
      },
      error => this.rootComponent.showError(error));
  }

  delete(id: any) {
    this.carService.delete(id).subscribe(() => {
      this.rootComponent.showSuccess("Car deleted");
      this.ngOnInit();
    }, (error) => this.rootComponent.showError(error));
  }

  confirmDelete(id: number, parametro: any) {
    this.confirmDialogService.confirm('Warning!', 'Do you really want to delete car ' + parametro + '?')
      .then(() => this.delete(id))
      .catch(() => console.log());
  }

}
