import { Observable, of } from 'rxjs';
import { Routes, ActivatedRouteSnapshot, Resolve, RouterModule } from '@angular/router';
import { Injectable, NgModule } from '@angular/core';
import { CarService } from 'src/app/services/carService';
import { Car } from 'src/app/domains/car';
import { CarsComponent } from './cars.component';
import { NewCarComponent } from '../new-car/new-car.component';

@Injectable({ providedIn: 'root' })
export class CarResolve implements Resolve<Car> {

    constructor(private service: CarService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.findById(id);
        }
        return of(new Car());
    }
}

export const routes: Routes = [
    {
        path: 'cars',
        component: CarsComponent,
    },
    {
        path: 'new-car',
        component: NewCarComponent,
    },
    {
        path: 'new-car/:id',
        component: NewCarComponent,
        resolve: {
            car: CarResolve
        }
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [ RouterModule ]
  })
export class CarRoutingModule { }
