import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Credentials } from 'src/app/domains/credentials';
import { AuthService } from 'src/app/services/authService';
import { NotificationService } from 'src/app/notification.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  creds : Credentials = {
    login: "",
    password: ""
  };

  constructor(
    private router: Router,
    private auth: AuthService,
    private rootComponent: NotificationService
    ) { }

  ngOnInit() {
  }

  login() {
     this.auth.authenticate(this.creds)
     .subscribe(response =>{
       this.auth.successfulLogin(response.headers.get('Authorization'));
       this.router.navigate(['/cars']);
     },
      error => {
        this.rootComponent.showError(error.error.message);
      }
     );
  };

}
