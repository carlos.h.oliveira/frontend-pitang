import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from 'src/app/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  constructor(private notifyService : NotificationService) { }

  ngOnInit() {
  }

  showToaster(){
    this.notifyService.showSuccess("Data shown successfully !!")
}

}
