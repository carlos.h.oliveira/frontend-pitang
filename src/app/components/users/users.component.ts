import { Component, OnInit } from '@angular/core';
import { User } from '../../domains/user'
import { UserService } from 'src/app/services/userService';
import { ConfirmDialogService } from '../confirm-dialog/confirm-dialog.service';
import { NotificationService } from 'src/app/notification.service';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  modalOptions:NgbModalOptions;
  closeResult: string;

  constructor(
    private userService: UserService,
    private confirmDialogService: ConfirmDialogService,
    private rootComponent: NotificationService,
    private modalService: NgbModal
  ) { 
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit() {
    this.userService.findAll().subscribe(
      data => {
        this.users = data;
      },
      error => this.rootComponent.showError(error));
  }

  delete(id: any) {
    this.userService.delete(id).subscribe(() => {
      this.rootComponent.showSuccess("User deleted");
      this.ngOnInit();
    }, (error) => this.rootComponent.showError(error));
  }

  confirmDelete(id: number, parametro: any) {
    this.confirmDialogService.confirm('Warning!', 'Do you really want to delete user ' + parametro + '?')
      .then(() => this.delete(id))
      .catch(() => console.log());
  }

  seeCars(cars: any){
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.title = 'Cars';
      modalRef.componentInstance.content = cars;
  }

}
