import { Observable, of } from 'rxjs';
import { Routes, ActivatedRouteSnapshot, Resolve, RouterModule } from '@angular/router';
import { Injectable, NgModule } from '@angular/core';
import { RegisterComponent } from '../register/register.component';
import { User } from 'src/app/domains/user';
import { UserService } from 'src/app/services/userService';
import { UsersComponent } from './users.component';

@Injectable({ providedIn: 'root' })
export class UserResolve implements Resolve<User> {

    constructor(private service: UserService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.findById(id);
        }
        return of(new User());
    }
}

export const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
        resolve: {
              user: UserResolve
          }
      },
    {
      path: 'register/:id',
      component: RegisterComponent,
      resolve: {
            user: UserResolve
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [ RouterModule ]
  })
export class UserRoutingModule { }
